/** Example adapted from https://github.com/msgpack/msgpack-c */

#include <msgpack.hpp>
#include <string>
#include <iostream>
#include <sstream>


std::string test_serialize(std::stringstream & buffer);
void test_deserialize(const std::string & str);

int main(void)
{
    std::cout<<"*** Running msgpack-c example ***" << std::endl;

    std::stringstream buffer;

    std::string str = test_serialize(buffer);

    test_deserialize(str);

    std::cout<<"*********************************" << std::endl;

    return 0;
}

std::string test_serialize(std::stringstream & buffer){

    msgpack::type::tuple<int, bool, std::string> src(1, true, "example");

    // serialize the object into the buffer.
    // any classes that implements write(const char*,size_t) can be a buffer.
    msgpack::pack(buffer, src);

    // send the buffer ...
    buffer.seekg(0);

    // deserialize the buffer into msgpack::object instance.
    std::cout << "serialized: \'" << buffer.str() << "\'" << std::endl;

    return buffer.str();
}

void test_deserialize(const std::string & str){
    msgpack::object_handle oh =
        msgpack::unpack(str.data(), str.size());

    // deserialized object is valid during the msgpack::object_handle instance is alive.
    msgpack::object deserialized = oh.get();

    // msgpack::object supports ostream.
    std::cout << "deserialized: " << deserialized << std::endl;
}
