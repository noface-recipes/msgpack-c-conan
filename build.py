from conan.packager import ConanMultiPackager

import os

if __name__ == "__main__":
    builder = ConanMultiPackager(build_types=["Release"])
    builder.add_common_builds(shared_option_name="msgpack-c:shared", pure_c=False)


    for settings, options, env_vars, build_requires in builder.builds:
        options["msgpack-c:header_only"] = "False"


    build_header_only = os.environ.get('BUILD_HEADER_ONLY', False)

    if build_header_only == "True" or build_header_only == True:
        builder.add(options={"msgpack-c:header_only" : "True"})

    builder.run()
