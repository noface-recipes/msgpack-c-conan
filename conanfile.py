from conans import ConanFile, CMake, tools
import os
from os import path

class msgpack(ConanFile):
    """
    msgpack is "MessagePack implementation for C and C++".
    See: https://github.com/msgpack/msgpack-c
    """

    name = "msgpack-c"
    version = "1.4.2"
    license="Boost Software License - Version 1.0. http://www.boost.org/LICENSE_1_0.txt"
    settings = "os", "compiler", "arch"

    options = {
        "header_only": [True, False],
        "shared"     : [True, False],
        "use_pic"    : ["default", True, False]
    }
    default_options = (
        "header_only=True",
        "shared=True",
        "use_pic=default"
    )

    generators = "cmake"
    exports = ("CMakeLists.txt",)

    REPO = "https://github.com/msgpack/msgpack-c"

    ZIP_URL = "%s/releases/download/cpp-%s/msgpack-%s.tar.gz" % (REPO, version, version)
    #Folder inside the zip
    ZIP_FOLDER_NAME = "msgpack-" + version
    UNZIPPED_DIR = "msgpack-c"
    FILE_SHA = 'c0f1da8462ea44b23f89573eff04a1f329bcff6fd80eb0d4b976d7f19caf1fa2'

    def configure(self):
        if self.options.header_only:
            self.output.info("Clearing settings")
            self.settings.clear()

            self.options.remove("shared")
            self.options.remove("use_pic")
        else:
            self.options["cmake_installer"].version = "3.6.3" # last version with linux x86 support

    def build_requirements(self):
        if not self.options.header_only:
            self.build_requires("cmake_installer/1.0@conan/stable")

    def source(self):
        zip_name = "%s.tar.gz" % self.name

        tools.download(self.ZIP_URL, zip_name)
        tools.check_sha256(zip_name, self.FILE_SHA)
        tools.untargz(zip_name)
        os.unlink(zip_name)

        os.rename(self.ZIP_FOLDER_NAME, self.UNZIPPED_DIR)

    def build(self):
        if self.options.header_only:
            return

        self.apply_patches()


        cmake = CMake(self)

        self.build_dir = os.path.join(self.build_folder, "build")
        self.cmake_configure(cmake, self.build_dir)
        self.cmake_build_and_install(cmake, self.build_dir)

    def package(self):
        self.copy("*", "include", path.join(self.UNZIPPED_DIR, 'include'))

    def package_info(self):
        if not self.options.header_only:
            self.cpp_info.libs = ["msgpackc"]

####################################### Helpers ################################################

    def apply_patches(self):
        line  = '    SET_PROPERTY (TARGET msgpackc APPEND_STRING PROPERTY COMPILE_FLAGS " -Wno-mismatched-tags")'
        patch = '    IF (MSGPACK_ENABLE_SHARED)\n' \
              + '    ' + line + '\n' \
              + '    ENDIF ()'

        tools.replace_in_file(path.join(self.UNZIPPED_DIR, "CMakeLists.txt"), line, patch)

    def cmake_configure(self, cmake, build_folder):
        cmake.configure(
            defs        = self.cmake_defs(),
            source_dir  = path.join(self.conanfile_directory, self.UNZIPPED_DIR),
            build_dir   = build_folder)

    def cmake_build_and_install(self, cmake, build_dir):
        cmd = 'cmake --build . --target install %s' % (cmake.build_config)

        self.output.info("cwd: '%s' cmd: '%s'" % (build_dir, cmd))

        self.run(cmd, cwd=build_dir)

    def cmake_defs(self):
        """Generate definitions for cmake"""

        if not hasattr(self, 'package_folder'):
            self.package_folder = path.join(self.build_folder, "dist")

        args = {
            'MSGPACK_CXX11'          : True,
            'CMAKE_INSTALL_PREFIX'   : self.package_folder,
            'MSGPACK_BUILD_EXAMPLES' : False
        }

        if self.options.header_only == False:
            args['MSGPACK_ENABLE_SHARED'] = self.options.shared

            if self.options.use_pic != "default":
                args['CMAKE_POSITION_INDEPENDENT_CODE'] = self.options.use_pic

        return args
